package com.vijay.pipeline;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import com.amazonaws.services.cloudformation.AmazonCloudFormation;
import com.amazonaws.services.cloudformation.AmazonCloudFormationClientBuilder;
import com.amazonaws.services.cloudformation.model.CreateStackRequest;
import com.amazonaws.services.cloudformation.model.DescribeStacksRequest;
import com.amazonaws.services.cloudformation.model.Parameter;
import com.amazonaws.services.cloudformation.model.Stack;
import com.amazonaws.services.cloudformation.model.StackStatus;
import com.vijay.pipeline.model.DataFeed;

@Service
public class pipelineServiceImpl implements pipelineService {

	Map<String, String> pipelines = new HashMap<>();

	@Override
	public Response createStack(DataFeed dataFeed) {

		AmazonCloudFormation stackbuilder = AmazonCloudFormationClientBuilder.standard().withRegion("us-east-2")
				.build();
		CreateStackRequest createRequest = new CreateStackRequest();
		createRequest.setStackName("CodeTest2");
		try {
			// Controller.class.getResourceAsStream(finaltemp.json)
			createRequest.setTemplateBody(convertStreamToString(new FileInputStream(new File(System.getProperty("user.dir")+
					"/src/main/resources/finaltemp.json"))));
		} catch (Exception e) {
			return Response.ok().build();
		}

		System.out.println("Creating a stack called " + createRequest.getStackName() + ".");
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter p = new Parameter();
		p.setParameterKey("SourceCodeBucket");
		p.setParameterValue(dataFeed.getBucketName());

		params.add(p);
		p = new Parameter();
		p.setParameterKey("arguments");
		p.setParameterValue(dataFeed.getArguments());
		params.add(p);
		createRequest.setParameters(params);
		List<String> caps = new ArrayList<>();
		caps.add("CAPABILITY_IAM");
		createRequest.setCapabilities(caps);
		stackbuilder.createStack(createRequest);
		try {
			System.out.println("Stack creation completed, the stack " + createRequest.getStackName()
					+ " completed with " + waitForCompletion(stackbuilder, createRequest.getStackName()));
		} catch (Exception e) {
			return Response.ok().build();
		}

		return Response.ok().build();
	}

	// Convert a stream into a single, newline separated string
	public static String convertStreamToString(InputStream in) throws Exception {

		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder stringbuilder = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			stringbuilder.append(line + "\n");
		}
		in.close();
		return stringbuilder.toString();
	}

	public static String waitForCompletion(AmazonCloudFormation stackbuilder, String stackName) throws Exception {

		DescribeStacksRequest wait = new DescribeStacksRequest();
		wait.setStackName(stackName);
		Boolean completed = false;
		String stackStatus = "Unknown";
		String stackReason = "";

		System.out.print("Waiting");

		while (!completed) {
			List<Stack> stacks = stackbuilder.describeStacks(wait).getStacks();
			if (stacks.isEmpty()) {
				completed = true;
				stackStatus = "NO_SUCH_STACK";
				stackReason = "Stack has been deleted";
			} else {
				for (Stack stack : stacks) {
					if (stack.getStackStatus().equals(StackStatus.CREATE_COMPLETE.toString())
							|| stack.getStackStatus().equals(StackStatus.CREATE_FAILED.toString())
							|| stack.getStackStatus().equals(StackStatus.ROLLBACK_COMPLETE.toString())
							|| stack.getStackStatus().equals(StackStatus.DELETE_FAILED.toString())) {
						completed = true;
						stackStatus = stack.getStackStatus();
						stackReason = stack.getStackStatusReason();
					}
				}
			}

			// Show we are waiting
			System.out.print(".");

			// Not done yet so sleep for 10 seconds.
			if (!completed)
				Thread.sleep(10000);
		}

		// Show we are done
		System.out.print("done\n");

		return stackStatus + " (" + stackReason + ")";
	}

}
