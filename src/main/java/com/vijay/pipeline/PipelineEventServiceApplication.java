package com.vijay.pipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PipelineEventServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PipelineEventServiceApplication.class, args);
	}
}
