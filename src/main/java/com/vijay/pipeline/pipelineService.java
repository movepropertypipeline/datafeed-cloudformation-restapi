package com.vijay.pipeline;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.vijay.pipeline.model.DataFeed;

@Consumes("application/json")
@Produces("application/json")
@Path("/pipelineservice")
public interface pipelineService {
	
	@Path("/create")
	@POST
	Response createStack(DataFeed dataFeed);

}
