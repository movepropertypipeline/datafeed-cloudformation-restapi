package com.vijay.pipeline.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Pipeline")
public class DataFeed {
	
	private String bucketName;
	private String arguments;
	
	public String getBucketName() {
		return bucketName;
	}
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	public String getArguments() {
		return arguments;
	}
	public void setArguments(String arguments) {
		this.arguments = arguments;
	}

}
